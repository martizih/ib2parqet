# Interactive Brokers to Parqet Converter

`ib2parqet` is a simple command-line utility that converts Flex-Query exports from Interactive Brokers into csv files that can be imported into Parqet.


## Installation

```sh
python -m pip install ib2parqet
```

## Usage

```sh
usage: ib2parqet [-h] [-v] [-o OUTPUT] input

positional arguments:
  input                 path to Flex Query xml generated in IB

options:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -o OUTPUT, --output OUTPUT
                        output file path
```
