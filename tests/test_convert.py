import os

import xmltodict

from src.ib2parqet.ib2parqet import convert

dirname = os.path.dirname(__file__)

with open(os.path.join(dirname, "forex.xml")) as fh:
    forex = xmltodict.parse(fh.read())
with open(os.path.join(dirname, "stocks.xml")) as fh:
    stocks = xmltodict.parse(fh.read())


def test_forex():
    assert convert(forex) == []


def test_stock():
    assert convert(stocks) == [
        {
            "broker": "Interactive Brokers",
            "currency": "USD",
            "datetime": "2022-06-22T03:07:02Z",
            "fee": 2.0888155,
            "fxrate": 0.96126,
            "isin": "IE00BFY0GT14",
            "price": 25.42,
            "shares": 145.0,
            "tax": 0.0,
            "type": "BUY",
        }
    ]
